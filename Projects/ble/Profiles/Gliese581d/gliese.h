#ifndef GLIESEPROFILE_H
#define GLIESEPROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

    /*********************************************************************
     * INCLUDES
     */

    /*********************************************************************
     * CONSTANTS
     */

    // Profile Parameters
#define GLIESEPROFILE_CHAR1                   0  // RW uint8 - Profile Characteristic 1 value
#define GLIESEPROFILE_CHAR2                   1  // RW uint8 - Profile Characteristic 2 value
#define GLIESEPROFILE_CHAR3                   2  // RW uint8 - Profile Characteristic 3 value
#define GLIESEPROFILE_CHAR4                   3  // RW uint8 - Profile Characteristic 4 value
#define GLIESEPROFILE_CHAR5                   4  // RW uint8 - Profile Characteristic 4 value
#define GLIESEPROFILE_CHAR6                   5
#define GLIESEPROFILE_CHAR7                   6  // To read CC2541 internal temperature.
#define GLIESEPROFILE_CHAR8                   7  // To read CC2541 internal temperature calibration.
#define GLIESEPROFILE_CHAR9                   8  // To read CC2541 internal temperature calibration.
#define GLIESEPROFILE_CHAR10                  9  // To write bond about.

    // Gliese Profile Service UUID
    // 013d8e3b-1877-4d5c-bc59-aaa7e5082346
#define GLIESEPROFILE_SERV_UUID 0x46, 0x23, 0x08, 0xe5, 0xa7, 0xaa, 0x59, 0xbc, 0x5c, 0x4d, 0x77, 0x18, 0x3b, 0x8e, 0x3d, 0x01

    // 153117a7-311b-4cc7-904a-820adc5d8461
#define GLIESEPROFILE_CHAR1_UUID 0x61, 0x84, 0x5d, 0xdc, 0x0a, 0x82, 0x4a, 0x90, 0xc7, 0x4c, 0x1b, 0x31, 0xa7, 0x17, 0x31, 0x15

    // 20490d79-99e1-4fc1-bba0-b43f88aafeb6
#define GLIESEPROFILE_CHAR2_UUID 0xb6, 0xfe, 0xaa, 0x88, 0x3f, 0xb4, 0xa0, 0xbb, 0xc1, 0x4f, 0xe1, 0x99, 0x79, 0x0d, 0x49, 0x20

    // 34e0bd91-5e47-42e2-acd4-d5b4c25070af
#define GLIESEPROFILE_CHAR3_UUID 0xaf, 0x70, 0x50, 0xc2, 0xb4, 0xd5, 0xd4, 0xac, 0xe2, 0x42, 0x47, 0x5e, 0x91, 0xbd, 0xe0, 0x34

    // 4e289120-d767-4aa5-ae43-db58671f6856
#define GLIESEPROFILE_CHAR4_UUID 0x56, 0x68, 0x1f, 0x67, 0x58, 0xdb, 0x43, 0xae, 0xa5, 0x4a, 0x67, 0xd7, 0x20, 0x91, 0x28, 0x4e

    // 51567b6d-a035-499d-a2ea-1e27b5ae8b37
#define GLIESEPROFILE_CHAR5_UUID 0x37, 0x8b, 0xae, 0xb5, 0x27, 0x1e, 0xea, 0xa2, 0x9d, 0x49, 0x35, 0xa0, 0x6d, 0x7b, 0x56, 0x51

    // 6ee16f2c-fcb7-4ca0-b416-7718a6f0687c
#define GLIESEPROFILE_CHAR6_UUID 0x7c, 0x68, 0xf0, 0xa6, 0x18, 0x77, 0x16, 0xb4, 0xa0, 0x4c, 0xb7, 0xfc, 0x2c, 0x6f, 0xe1, 0x6e,

    // 7fc69075-e330-4db6-8c46-8a8219eb7448
#define GLIESEPROFILE_CHAR7_UUID 0x48, 0x74, 0xeb, 0x19, 0x82, 0x8a, 0x46, 0x8c, 0xb6, 0x4d, 0x30, 0xe3, 0x75, 0x90, 0xc6, 0x7f

    // 8fa6fc8c-4d73-4aa7-9097-30d0e33b79e3
#define GLIESEPROFILE_CHAR8_UUID 0xe3, 0x79, 0x3b, 0xe3, 0xd0, 0x30, 0x97, 0x90, 0xa7, 0x4a, 0x73, 0x4d, 0x8c, 0xfc, 0xa6, 0x8f

    // 9bf340db-0153-41e3-a046-bf3f3c72cac7
#define GLIESEPROFILE_CHAR9_UUID 0xc7, 0xca, 0x72, 0x3c, 0x3f, 0xbf, 0x46, 0xa0, 0xe3, 0x41, 0x53, 0x01, 0xdb, 0x40, 0xf3, 0x9b

    // ab7b42fc-9dfb-438a-9ec0-7c579843e9e2
#define GLIESEPROFILE_CHAR10_UUID 0xe2, 0xe9, 0x43, 0x98, 0x57, 0x7c, 0xc0, 0x9e, 0x8a, 0x43, 0xfb, 0x9d, 0xfc, 0x42, 0x7b, 0xab,


    // Gliese Profile Services bit fields
#define GLIESEPROFILE_SERVICE               0x00000001

    // Length of Characteristic 1 in bytes
#define GLIESEPROFILE_CHAR1_LEN           11 // The last four bytes is passcode
    // Length of Characteristic 2 in bytes
#define GLIESEPROFILE_CHAR2_LEN           16
    // Length of Characteristic 5 in bytes
#define GLIESEPROFILE_CHAR5_LEN           2
#define GLIESEPROFILE_CHAR6_LEN           7 // The first six bytes is passcode
#define GLIESEPROFILE_CHAR7_LEN           9 // The first four bytes is passcode
#define GLIESEPROFILE_CHAR8_LEN           16 // The first byte is passcode
#define GLIESEPROFILE_CHAR9_LEN           16 // The first byte is passcode

#define GLIESEPROFILE_CHAR10_LEN          14 // The first six bytes is passcode
    /*********************************************************************
     * TYPEDEFS
     */


    /*********************************************************************
     * MACROS
     */

    /*********************************************************************
     * Profile Callbacks
     */

    // Callback when a characteristic value has changed
    typedef void (*glieseProfileChange_t)( uint8 paramID );

    typedef struct
    {
        glieseProfileChange_t        pfnGlieseProfileChange;  // Called when characteristic value changes
    } glieseProfileCBs_t;



    /*********************************************************************
     * API FUNCTIONS
     */


    /*
     * GlieseProfile_AddService- Initializes the Simple GATT Profile service by registering
     *          GATT attributes with the GATT server.
     *
     * @param   services - services to add. This is a bit map and can
     *                     contain more than one service.
     */

    extern bStatus_t GlieseProfile_AddService( uint32 services );

    /*
     * GlieseProfile_RegisterAppCBs - Registers the application callback function.
     *                    Only call this function once.
     *
     *    appCallbacks - pointer to application callbacks.
     */
    extern bStatus_t GlieseProfile_RegisterAppCBs( glieseProfileCBs_t *appCallbacks );

    /*
     * GlieseProfile_SetParameter - Set a Simple GATT Profile parameter.
     *
     *    param - Profile parameter ID
     *    len - length of data to right
     *    value - pointer to data to write.  This is dependent on
     *          the parameter ID and WILL be cast to the appropriate
     *          data type (example: data type of uint16 will be cast to
     *          uint16 pointer).
     */
    extern bStatus_t GlieseProfile_SetParameter( uint8 param, uint8 len, void *value );

    /*
     * GlieseProfile_GetParameter - Get a Simple GATT Profile parameter.
     *
     *    param - Profile parameter ID
     *    value - pointer to data to write.  This is dependent on
     *          the parameter ID and WILL be cast to the appropriate
     *          data type (example: data type of uint16 will be cast to
     *          uint16 pointer).
     */
    extern bStatus_t GlieseProfile_GetParameter( uint8 param, void *value );


    /*********************************************************************
    *********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* GLIESEPROFILE_H */
