/**************************************************************************************************
  Filename:       advs.h
  Revised:        $Date: 2015-10-13 17:45:58 +0800 $
  Revision:       $Revision: 0 $

  Description:    All Advertising beacon data for WeiXin Yao Yi Yao
**************************************************************************************************/

#ifndef ADVS_H
#define ADVS_H

#include "hal_types.h"

extern const uint8 aBeacon_195777[];
extern const uint8 aBeacon_195778[];
extern const uint8 aBeacon_195779[];
extern const uint8 aBeacon_195780[];
extern const uint8 aBeacon_195781[];
extern const uint8 aBeacon_195782[];
extern const uint8 aBeacon_195783[];
extern const uint8 aBeacon_195784[];
extern const uint8 aBeacon_195785[];
extern const uint8 aBeacon_195786[];
extern const uint8 aBeacon_195787[];
extern const uint8 aBeacon_195788[];
extern const uint8 aBeacon_195789[];
extern const uint8 aBeacon_195790[];
extern const uint8 aBeacon_195791[];
extern const uint8 aBeacon_195792[];
extern const uint8 aBeacon_195793[];

extern const uint8 aBeacon_733639[];
extern const uint8 aBeacon_733640[];
extern const uint8 aBeacon_733641[];
extern const uint8 aBeacon_733642[];
extern const uint8 aBeacon_733643[];
extern const uint8 aBeacon_733644[];
extern const uint8 aBeacon_733645[];
extern const uint8 aBeacon_733646[];
#endif
